# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from sql import Null, Cast, Literal
from sql.aggregate import Sum, Max, Min
from sql.functions import DatePart, DateTrunc, Function
from trytond.model import fields, ModelView, ModelSQL
from trytond.pool import PoolMeta, Pool
from trytond.pyson import PYSONEncoder
from trytond.tools import grouped_slice, reduce_ids
from trytond.transaction import Transaction
from trytond.wizard import (Wizard, StateAction, StateTransition,
    StateView, Button)
from trytond import backend

__all__ = ['Combined', 'UlsByLocationCombinedParam',
           'UlsByLocationCombinedWizard', 'UlsByLocationCombined']


class SQLiteDatePart(Function):
    __slots__ = ()
    _function = 'STRFTIME'


class Combined(metaclass=PoolMeta):
    __name__ = 'stock.location.combined'

    ul_quantity = fields.Function(
        fields.Float('ULs', digits=(16, 0)), 'get_ul_quantity')

    @classmethod
    def get_ul_quantity(cls, records, name=None):
        pool = Pool()
        Unitload = pool.get('stock.unit_load')
        unit_load = Unitload.__table__()

        ids = [r.id for r in records]
        quantities = dict.fromkeys(ids, None)
        if not Transaction().context.get('production_date'):
            return quantities

        cursor = Transaction().connection.cursor()
        where = cls._get_ul_quantity_where([unit_load])
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(unit_load.location_combined, sub_ids)
            cursor.execute(*unit_load.select(
                unit_load.location_combined,
                Sum(Literal(1)),
                where=red_sql & where,
                group_by=unit_load.location_combined))

            for loc_id, quantity in cursor.fetchall():
                quantities[loc_id] = quantity

        return quantities

    @classmethod
    def _get_ul_quantity_where(cls, tables):
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')

        type_date = Move.planned_date.sql_type().base
        _date = Transaction().context.get('production_date', Date.today())
        where = ((tables[0].start_date.cast(type_date) == _date) | (
            tables[0].end_date.cast(type_date) == _date))
        where &= tables[0].location_combined != Null
        return where


class UlsByLocationCombinedParam(ModelView):
    """ULs by location combined data"""
    __name__ = 'stock.unit_load_by_location_combined.param'

    date = fields.Date('Date', required=True)
    grouping = fields.Selection([('hour', 'Hour')], 'Grouping',
                                required=True)

    @staticmethod
    def default_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @staticmethod
    def default_grouping():
        return 'hour'


class UlsByLocationCombinedWizard(Wizard):
    """ULs by location combined"""
    __name__ = 'stock.unit_load_by_location_combined'

    start = StateView('stock.unit_load_by_location_combined.param',
        'stock_unit_load_location_combined.ul_by_combined_param_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Open', 'pre_open_', 'tryton-ok', default=True)])
    pre_open_ = StateTransition()
    open_ = StateAction(
        'stock_unit_load_location_combined.act_ul_by_combined_board')

    def transition_pre_open_(self):
        return 'open_'

    def do_open_(self, action):
        return self._do_open_(action)

    def _do_open_(self, action):
        pool = Pool()
        Lang = pool.get('ir.lang')
        for code in [Transaction().language, 'en_US']:
            langs = Lang.search([('code', '=', code)])
            if langs:
                break
        lang, = langs
        date = lang.strftime(self.start.date)
        action['pyson_context'] = PYSONEncoder().encode(
            self._get_open_context())
        action['name'] += ' @ %s' % date
        return action, {}

    def _get_open_context(self):
        return {'production_date': self.start.date,
                'production_type': 'combined',
                'ul_grouping': self.start.grouping}

    def transition_open_(self):
        return 'end'


class UlsByLocationCombined(ModelSQL, ModelView):
    """ULs by location combined"""
    __name__ = 'stock.unit_load_by_location_combined'

    location_combined = fields.Many2One('stock.location.combined',
                                        'Location combined', readonly=True)
    datetime_ = fields.DateTime('Date', readonly=True)
    start_time = fields.Integer('Start time', readonly=True,
                                help='Start time in minutes')
    end_time = fields.Integer('End time', readonly=True,
                              help='End time in minutes')
    ul_quantity = fields.Float('ULs', digits=(16, 0), readonly=True)
    ul_cumulate = fields.Function(
        fields.Float('Cumulate', digits=(16, 0), readonly=True),
        'get_ul_cumulate')
    ul_average = fields.Function(
        fields.Float('Average', digits=(16, 3), readonly=True),
        'get_ul_average')

    @classmethod
    def table_query(cls):
        pool = Pool()
        Unitload = pool.get('stock.unit_load')
        Combined = pool.get('stock.location.combined')
        unit_load = Unitload.__table__()

        if backend.name != 'sqlite':
            date = DatePart('hour', unit_load.end_date)
        else:
            date = Cast(SQLiteDatePart('%H', unit_load.end_date), 'int')
        where = Combined._get_ul_quantity_where([unit_load])
        columns, grouping = cls._get_query_columns([unit_load])
        query = unit_load.select(*columns,
            where=where,
            group_by=grouping,
            order_by=(unit_load.location_combined,
                date))
        return query

    @classmethod
    def get_ul_cumulate(cls, records, name=None):
        to_cumulate = sorted(records,
            key=lambda x: (x.location_combined, x.datetime_))

        qty = 0
        res = dict.fromkeys(list(map(int, records)), 0)
        for item in to_cumulate:
            qty += item.ul_quantity
            res[item.id] = qty
        return res

    @classmethod
    def get_ul_average(cls, records, name=None):
        Conf = Pool().get('stock.configuration')
        conf = Conf(1)

        to_cumulate = sorted(records,
            key=lambda x: (x.location_combined, x.datetime_))

        qty = 0
        res = dict.fromkeys(list(map(int, records)), 0)
        counter = 0.0
        for item_id, item in enumerate(to_cumulate):
            counter += 1
            if (conf.combined_start_time_limit
                    and item.start_time > conf.combined_start_time_limit):
                counter -= ((float(conf.combined_start_time_limit) / 60.0)
                    * int(item.start_time / conf.combined_start_time_limit))
            elif (conf.combined_start_time_limit
                    and item.end_time < conf.combined_start_time_limit):
                counter -= ((float(conf.combined_start_time_limit) / 60.0)
                    * int((item.end_time + conf.combined_start_time_limit)
                        / conf.combined_start_time_limit))
            qty += item.ul_quantity
            res[item.id] = qty / counter
        return res

    @classmethod
    def _get_query_columns(cls, tables):
        max_range = 24
        type_name = cls.id.sql_type().base
        if backend.name != 'sqlite':
            hour = DatePart('hour', tables[0].end_date)
            minute = DatePart('minutes', tables[0].end_date)
        else:
            hour = SQLiteDatePart('%H', tables[0].end_date)
            minute = Cast(SQLiteDatePart('%M', tables[0].end_date), 'int')
        return (
            [((Literal(max_range) * tables[0].location_combined)
             - (Literal(max_range) - hour.cast(type_name))).as_('id'),
             Max(tables[0].create_uid).as_('create_uid'),
             Max(tables[0].create_date).as_('create_date'),
             Max(tables[0].write_uid).as_('write_uid'),
             Max(tables[0].write_date).as_('write_date'),
             tables[0].location_combined,
             DateTrunc('hour', tables[0].end_date).as_('datetime_'),
             Sum(Literal(1)).as_('ul_quantity'),
             Min(minute).as_('start_time'),
             Max(minute).as_('end_time')],
            [hour, DateTrunc('hour', tables[0].end_date),
             tables[0].location_combined])
