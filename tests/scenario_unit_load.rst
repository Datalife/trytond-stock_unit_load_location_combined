========================
Check unit load creation
========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.stock_location_combined.tests.tools \
    ...     import get_combined_location
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)

Install unit load Module::

    >>> config = activate_modules('stock_unit_load_location_combined')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])

Create location combined::

    >>> combined = get_combined_location()

Create unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.production_type = 'combined'
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.location_combined = combined
    >>> unit_load.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.RequiredValidationError: A value is required for field "Warehouse" in "Unit load". - 
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.save()
    >>> not unit_load.code
    False

Add moves::

    >>> unit_load.production_state
    'running'
    >>> unit_load.quantity = Decimal('36.0')
    >>> len(unit_load.production_moves)
    2
    >>> [m.from_location.id for m in unit_load.production_moves] == [l.id for l in combined.locations]
    True
    >>> unit_load.production_moves[0].quantity
    18.0
    >>> sum(m.quantity for m in unit_load.production_moves)
    36.0
    >>> unit_load.production_moves[0].to_location = storage_loc
    >>> unit_load.production_moves[1].to_location = storage_loc
    >>> unit_load.save()

Production locations::

    >>> unit_load.reload()
    >>> len(unit_load.production_locations)
    2
