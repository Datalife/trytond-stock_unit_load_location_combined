# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .unit_load import UnitLoad, UnitLoadLabel
from .location import (Combined, UlsByLocationCombinedParam,
                       UlsByLocationCombinedWizard,
                       UlsByLocationCombined)
from .configuration import Configuration, ConfigurationULProductionType


def register():
    Pool.register(
        Configuration,
        UnitLoad,
        Combined,
        UlsByLocationCombinedParam,
        UlsByLocationCombined,
        ConfigurationULProductionType,
        module='stock_unit_load_location_combined', type_='model')
    Pool.register(
        UlsByLocationCombinedWizard,
        module='stock_unit_load_location_combined', type_='wizard')
    Pool.register(
        UnitLoadLabel,
        module='stock_unit_load_location_combined', type_='report')
