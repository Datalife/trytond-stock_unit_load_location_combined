# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.tools import grouped_slice, reduce_ids
from trytond.transaction import Transaction

__all__ = ['UnitLoad', 'UnitLoadLabel']


# TODO: filter location_combined for warehouse_production
# TODO: in explode_prod_moves filter location by warehouse prod
class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    location_combined = fields.Many2One('stock.location.combined',
        'Location combined',
        ondelete='RESTRICT', select=True,
        states={
            'readonly': (Eval('state') != 'draft') | (Eval(
                'production_state') == 'done'),
            'required': Eval('production_type') == 'combined',
            'invisible': Eval('production_type') != 'combined'
        },
        depends=['production_state', 'production_type', 'state'])
    production_locations = fields.Function(
        fields.Many2Many('stock.location', None, None, 'Production locations'),
        'get_production_locations')

    @classmethod
    def __setup__(cls):
        super(UnitLoad, cls).__setup__()
        new_arg = ('combined', 'Combined location')
        if new_arg not in cls.production_type._field.selection:
            cls.production_type._field.selection.append(new_arg)
        if cls.warehouse.states.get('required'):
            cls.warehouse.states['required'] |= Eval(
                'production_type') == 'combined'
        else:
            cls.warehouse.states['required'] = Eval(
                'production_type') == 'combined'
        if 'production_type' not in cls.warehouse.depends:
            cls.warehouse.depends.append('production_type')

    def get_production_type(self, name=None):
        if self.location_combined:
            return 'combined'
        return super(UnitLoad, self).get_production_type(name)

    @classmethod
    def get_production_locations(cls, records, name=None):
        pool = Pool()
        Move = pool.get('stock.move')
        Location = pool.get('stock.location')

        move = Move.__table__()
        ul = cls.__table__()
        location = Location.__table__()
        cursor = Transaction().connection.cursor()

        values = {r.id: [] for r in records}
        for sub_ids in grouped_slice([r.id for r in records]):
            red_sql = reduce_ids(ul.id, sub_ids)
            cursor.execute(*move.join(ul, condition=(
                (move.unit_load == ul.id) & (move.product == ul.product))
                ).join(location, condition=(move.from_location == location.id)
                ).select(ul.id, move.from_location,
                    where=(red_sql & (location.type == 'production')
                        & (move.end_date == ul.end_date)
                        & (move.state != 'cancelled')))
            )
            for ul, loc in cursor.fetchall():
                values[ul].append(loc)

        return values

    def get_production_moves(self, name=None):
        if self.production_type != 'combined':
            return super(UnitLoad, self).get_production_moves(name)
        if all(m.state == 'draft' for m in self.moves):
            loc_ids = [l.id for l in self.location_combined.locations]
        else:
            loc_ids = [l.id for l in self.production_locations]
        if self.warehouse_production:
            loc_ids.append(self.warehouse_production.id)
        moves = [m for m in self.moves
            if (m.from_location.type == 'production'
                and m.from_location.id in loc_ids
                and m.to_location.type == 'storage')
                or (m.to_location.id in loc_ids
                and m.product.id != self.product.id
                and m.from_location.type == 'storage')] or []
        if any(m.state == 'done' for m in moves):
            moves = [m for m in moves if m.start_date == self.start_date] or []
        if moves:
            return list(map(int, moves))
        return []

    def _check_production_data(self):
        res = super(UnitLoad, self)._check_production_data()
        if not res:
            return res
        return not (self.production_type == 'combined'
            and not self.location_combined)

    @fields.depends('location_combined')
    def on_change_product(self):
        super(UnitLoad, self).on_change_product()

    @fields.depends(methods=['explode_production_moves'])
    def on_change_location_combined(self):
        self.explode_production_moves()

    @ModelView.button_change('location_combined')
    def reset_production_moves(self):
        super(UnitLoad, self).reset_production_moves()

    @fields.depends('location_combined', 'production_type',
        'production_moves', 'product', methods=['_get_production_move'])
    def _explode_production_moves(self):

        super()._explode_production_moves()

        if self.production_type != 'combined':
            return
        if not self.production_moves:
            return
        value = list(self.production_moves)
        moves = [m for m in value if m.product.id == self.product.id]
        if len(moves) > 1:
            return
        moves, = moves
        locations = self.location_combined._get_locations()
        if not locations:
            return
        moves.from_location = locations[0]
        additional_qty = 0
        for location in locations[1:]:
            qty = moves.uom.round(moves.quantity / len(locations))
            new_move = self._get_production_move()
            new_move.from_location = location
            new_move.quantity = qty
            additional_qty += qty
            value.append(new_move)
        moves.quantity = moves.uom.round(moves.quantity - additional_qty)
        self.production_moves = value

    @classmethod
    def _get_production_type_fields(cls):
        res = super(UnitLoad, cls)._get_production_type_fields()
        res.update({'combined': 'location_combined'})
        return res


class UnitLoadLabel(metaclass=PoolMeta):
    __name__ = 'stock.unit_load.label'

    @classmethod
    def extra_info(cls, unit_load, language):
        res = super(UnitLoadLabel, cls).extra_info(unit_load, language)
        with Transaction().set_context(language=language):
            if unit_load.warehouse and unit_load.location_combined:
                res.append((unit_load.location_combined.rec_name, ''))
        return res
